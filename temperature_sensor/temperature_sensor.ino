// Pin-ul de info e pus pe analog0.

const int sensorPin = A0;
float voltage = 0;
float temperature = 0;

void setup() {
  Serial.begin(9600);
}

void loop() {
  int sensorValue = analogRead(sensorPin);

  Serial.print("Sensor value: ");
  Serial.print(sensorValue);

  voltage = (sensorValue / 1024.0) * 5.0;
  temperature = (voltage - 0.5) * 100;

  Serial.print(", Volts: ");
  Serial.print(voltage);

  Serial.print(", Temperature: ");
  Serial.println(temperature);

  delay(1);
}
