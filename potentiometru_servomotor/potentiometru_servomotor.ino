#include <Servo.h>

const int sensorPin = A0;
const int servoPin = 9;

Servo servo;
int angle = 0;

void setup() {
  Serial.begin(9600);
  servo.attach(9);
}

void loop() {
  // Get value and compute the angle
  int sensorValue = analogRead(sensorPin);
  angle = sensorValue - 1023 + 90;
  
  if (angle < 0) {
    angle = 0;  
  }
  Serial.println(angle);


  // Move the servo motor
  servo.write(angle*2);


  delay(15);
}
